#version 3.7;

#include "colors.inc"

#declare skyline_start = 0.0;
#declare skyline_end = 0.2;
#declare swirl_start = 0.25;
#declare swirl_end = 0.45;
#declare squares_start = 0.5;
#declare squares_end = 0.7;
#declare letters_start = 0.75;
#declare letters_end = 0.95;
#declare sponsors_start = 1.1;
#declare sponsors_end = 1.5;

camera {
#if (clock > swirl_end)
	orthographic
#end
	location < 295, 500, 250>
	look_at < 295, 0, 250 >
        sky <0, 1, 0>
}

global_settings {
	ambient_light <10, 10, 10>
}

#if (clock < sponsors_start)
#declare miniconf_object_finish = finish { };
#include "miniconf.pov"
object { skyline }

#debug concat("clock: ", str(clock, 8, 7), "\n")

#if (clock < skyline_end)
#debug "skyline\n"
box {
	<clock * (1 / skyline_end) * 576, 0, 0>, <600, 100, 800> 
	pigment { color White }
	finish { ambient <0.2, 0.2, 0.2> }
}
#end

#if (clock > swirl_start)
#debug "swirl\n"

object {
	swirl
#if (clock < swirl_end)
	translate < 0, 501 - (clock - swirl_start) * (1 / (swirl_end - swirl_start)) * 501, 0 >
#end
}

#end

#if (clock > squares_start)
#debug "squares\n"

#for (i, 0, 7)
  #if (((clock - squares_start) * (1 / (squares_end - squares_start)) * 8) > i)
    object {
      squares[i]
    }
  #end
#end

#end

#if (clock > letters_start)
#debug "letters\n"

#for (i, 0, 20)
  #if (((clock - letters_start) * (1 / (letters_end - letters_start)) * 20) > i)
    object {
      letters[i]
    }
  #end
#end

#end

#end

#if (clock >= sponsors_start)
#debug "sponsors"
#declare hpe4pov_object_finish = finish { };
#include "hpe.pov"

#switch (clock)

#range (sponsors_start, sponsors_start + (sponsors_end - sponsors_start) / 2)

object {
   hpe4pov_object
}

#break
#range (sponsors_start + (sponsors_end - sponsors_start) / 2, sponsors_end)
box {
  <0, 0, 0>, <1, 1, 1>
  pigment {
    image_map {
      png "linuxwochen.png"
    }
  }
  transform {
    scale <650, 309, 1>
    rotate <90, 0, 0>
    translate <-30, 0, 100>
  }
}
#end
#end

sky_sphere {
	pigment {
		color White
	}
}
