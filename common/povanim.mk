START := $(shell grep 'Initial_Frame' $(INIFILE) | cut -d'=' -f2)
STOP := $(shell grep 'Final_Frame' $(INIFILE) | cut -d'=' -f 2)
LEN := $(shell echo -n "$(STOP)" | wc -m)
ifeq ($(SGE_TASK_ID),)
NUMBERS := $(shell seq -w $(START) $(STOP) )
else
NUMBERS := $(shell seq -f '%0$(LEN).0f' $(SGE_TASK_ID) $(shell echo $$(( $(SGE_TASK_ID) + $(SGE_TASK_STEPSIZE) - 1 )) ) )
endif
POVFILE := $(shell grep 'Input_File_Name' $(INIFILE) | cut -d'"' -f 2)
OUTPUT := $(basename $(shell grep 'Output_File_Name' $(INIFILE) | cut -d'"' -f 2))
PNGFILES := $(foreach nr, $(NUMBERS), $(OUTPUT)$(nr).png)
POVSTATEFILES := $(foreach nr, $(NUMBERS), $(OUTPUT)$(nr).pov-state)

NSLOTS ?= 1
DISP ?= -D

$(OUTPUT)%.png : ONAME := $(OUTPUT)
$(OUTPUT)%.png : INAME := $(INIFILE)
$(OUTPUT)%.png: $(POVFILE) $(INIFILE) $(ASSETS)
	povray $(INAME) +WT$(NSLOTS) $(DISP) +SF'$*' +EF'$*' +O$(ONAME)

ifeq ($(SGE_TASK_ID),)
$(OUTPUT).dv : PATTERN := $(OUTPUT)%$(LEN)d
$(OUTPUT).dv : $(PNGFILES)
	ffmpeg -y -f image2 -i '$(PATTERN)'.png -target pal-dv $@
$(OUTPUT).ogv : PATTERN := $(OUTPUT)%$(LEN)d
$(OUTPUT).ogv : $(PNGFILES)
	ffmpeg2theora -f image2 -i '$(PATTERN)'.png -o $@
$(OUTPUT).webm : PATTERN := $(OUTPUT)%$(LEN)d
$(OUTPUT).webm : $(PNGFILES)
	ffmpeg -y -f image2 -i '$(PATTERN)'.png -codec libvpx -pass 1 $@
	ffmpeg -y -f image2 -i '$(PATTERN)'.png -codec libvpx -pass 2 $@

$(OUTPUT).vp9.webm : PATTERN := $(OUTPUT)%$(LEN)d
$(OUTPUT).vp9.webm: $(PNGFILES)
	ffmpeg -y -f image2 -i '$(PATTERN)'.png -codec libvpx-vp9 -pass 1 $@
	ffmpeg -y -f image2 -i '$(PATTERN)'.png -codec libvpx-vp9 -pass 2 $@

$(OUTPUT)_png.tgz: $(PNGFILES)
	tar cvzf $@ $(PNGFILES)
else
$(OUTPUT).dv: $(PNGFILES)
$(OUTPUT).ogv: $(PNGFILES)
$(OUTPUT).webm: $(PNGFILES)
$(OUTPUT).vp9.webm: $(PNGFILES)
$(OUTPUT)_png.tgz: $(PNGFILES)
endif

# We can only have the "clean:" and "reallyclean:" target once, so
# define them only when CLEANFILES has not been defined yet. Otherwise
# we can't include this file more than once, and we want to be able to
# do that.
ifeq ($(CLEANFILES),)
clean:
	$(RM) $(CLEANFILES)
reallyclean: clean
	$(RM) $(PRECIOUS)
endif

CLEANFILES := $(CLEANFILES) $(PNGFILES) $(POVSTATEFILES)
PRECIOUS := $(PRECIOUS) $(OUTPUT).dv $(OUTPUT).ogv $(OUTPUT).webm $(OUTPUT).vp9.webm $(OUTPUT)_png.tgz
