#macro Subsceneclock(Sceneclock, Substart, Subend)
((Sceneclock - Substart) * (1 / (Subend - Substart)))
#end
#macro Sceneclock(SceneStart, SceneEnd)
Subsceneclock(clock, SceneStart, SceneEnd)
#end
