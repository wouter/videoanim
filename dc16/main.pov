#version 3.7;

#include "colors.inc"
#include "textures.inc"
#include "shapes.inc"
#include "metals.inc"
#include "skies.inc"

#include "../common/common.inc"

#declare swirl_start = 0.0;
#declare swirl_end = 0.3;
#declare year_start = .275;
#declare year_end = .35;
#declare debconf_start = .4;
#declare debconf_end = .55;
#declare capetown_start = .56;
#declare capetown_end = .6;
#declare scale_start = .75;
#declare scale_end = .9;
#declare sponsors_start = .9;
#declare sponsors_end = 1.45;

#declare AllShapes_Finish = finish {
           phong 0.0
           reflection 0.0
           specular 0.0
}

#include "logo_dc16.pov"

camera {
#declare camera_zloc = -(AllShapes_WIDTH * 0.8);
    location <0, 0, camera_zloc>
    look_at  <0, 0, 0>
    right x*image_width/image_height
}

#if (clock < sponsors_end)
#declare scale_clock = max(min(Sceneclock(scale_start, scale_end), 1), 0);
#else
#declare scale_clock = 1 - Sceneclock(sponsors_end, 1.5);
#end

//light_source { <-200,   1, -8000> color White}
light_source { < 200, 100,  -8000> color rgb<.5,.5,.5> shadowless }

sky_sphere { pigment { color White } }

object {
  plane { <0, 0, 10>, 0 }
  texture {
    pigment { backdrop_COLOR transmit scale_clock }
  }
}

#if (clock < sponsors_end)
object {
#debug "plane\n"
    union{
      union{
#debug "swirls\n"
#declare swclock = Sceneclock(swirl_start, swirl_end);
        object { swirl1
          translate <0, (1 - min(swclock, 1)) * AllShapes_WIDTH * 1.3 * 60, 0>
        }
        object { swirl2
          translate <0, (1 - min(swclock, 1)) * AllShapes_WIDTH * 1.3 * 60, 0>
        }
        texture {
          pigment { swirl2_COLOR }
          finish { AllShapes_Finish }
        }
        scale <1, .01, 1>
      }
#if (clock >= year_start)
#debug "year\n"
      union {
        object { year_1 }
        object { year_6 }
        texture {
          #declare year_clock = Sceneclock(year_start, year_end);
          pigment {
#if (clock < year_end)
            color year_6_COLOR filter 0 transmit (1 - year_clock)
#else
            color year_6_COLOR filter 0 transmit 0
#end
          }
          finish { AllShapes_Finish }
        }
        scale <1, .01, 1>
        translate<0, .02, 0>
      }
#debug "debconf\n"
#if (clock >= debconf_start)
#declare dc_clock = Sceneclock(debconf_start, debconf_end);
#debug "d\n"
#declare dc_d_clock = Subsceneclock(dc_clock,  0, .2);
#debug "e\n"
#declare dc_e_clock = Subsceneclock(dc_clock, .1, .3);
#debug "b\n"
#declare dc_b_clock = Subsceneclock(dc_clock, .2, .4);
#debug "c\n"
#declare dc_c_clock = Subsceneclock(dc_clock, .3, .5);
#debug "o\n"
#declare dc_o_clock = Subsceneclock(dc_clock, .4, .6);
#debug "n\n"
#declare dc_n_clock = Subsceneclock(dc_clock, .5, .7);
#debug "f\n"
#declare dc_f_clock = Subsceneclock(dc_clock, .6, .8);

#macro debconfletter(lclock, lobj)
#if (lclock > 0)
        object {
          lobj
#if (lclock < 1)
          translate<0, (1 - lclock) * camera_zloc, 0>
#end
          texture {
            pigment {
#if (lclock < 1)
              color debconf_d_COLOR filter 0 transmit (1 - lclock)
#else
              color debconf_d_COLOR filter 0 transmit 0
#end
            }
            finish { AllShapes_Finish }
          }
        }
#end
#end
      union {
        debconfletter(dc_d_clock, debconf_d)
        debconfletter(dc_e_clock, debconf_e)
        debconfletter(dc_b_clock, debconf_b)
        debconfletter(dc_c_clock, debconf_c)
        debconfletter(dc_o_clock, debconf_o)
        debconfletter(dc_n_clock, debconf_n)
        debconfletter(dc_f_clock, debconf_f)
        scale <1, .03, 1>
      }
#if (clock > capetown_start)
      union {
        union {
          object { capetown_c }
          object { capetown_a }
          object { capetown_p }
          object { capetown_e }
          texture {
            pigment { capetown_c_COLOR }
            finish { AllShapes_Finish }
          }
        }
#if (clock > capetown_end)
        union {
          object { capetown_t }
          object { capetown_o }
          object { capetown_w }
          object { capetown_n }
          texture {
            pigment { capetown_c_COLOR }
            finish { AllShapes_Finish }
          }
        }
#end
        scale <1, .03, 1>
      }
#end
#end
#end
    }

    translate<-AllShapes_CENTER_X, 0, -AllShapes_CENTER_Y>
    rotate <-90,   0,   0>
#if(clock >= scale_start)
#declare scale_spline = spline {
#debug concat(str(clock, 0, -1), "=>", str(scale_clock, 0, -1), "\n")
    linear_spline
    0, <1, 1, 1>
    1, <.2, .2, 1>
};
    scale scale_spline(scale_clock)
#declare translate_spline = spline {
    linear_spline
    0, <0, 0, 0>
    1, <-(((image_width/2) - (AllShapes_WIDTH * .2)/2)/3.5), ((image_height/2) - (AllShapes_HEIGHT * .2))/3.5, 0>
};
    translate translate_spline(scale_clock)
#end
}

#if (clock >= sponsors_start)
#switch(clock)
#range (sponsors_start, sponsors_start + (sponsors_end - sponsors_start) / 2)
#declare hpe4pov_object_finish = finish { };
#include "hpe.pov"
object {
    hpe4pov_object
    translate -((max_extent(hpe4pov_object) - min_extent(hpe4pov_object)) / 2)
    rotate x*-90
    scale .4
    translate y*-75
}
#break
#range (sponsors_start + (sponsors_end - sponsors_start) / 2, sponsors_end)
#declare logo_box_side = (image_height / 8);
#include "google.pov"
object {
    AllShapes_Z
    translate<-AllShapes_CENTER_X, 0, -AllShapes_CENTER_Y>
    rotate <-90,0,0>
    scale logo_box_side / AllShapes_WIDTH
    translate<-logo_box_side*.6, logo_box_side*.6, 0>
}
#include "ibm.pov"
object {
    AllShapes_Z
    translate<-AllShapes_CENTER_X, 0, -AllShapes_CENTER_Y>
    rotate <-90,0,0>
    scale logo_box_side / AllShapes_WIDTH
    translate<logo_box_side*.6, logo_box_side*.6, 0>
}
#include "collabora.pov"
object {
    AllShapes_Z
    translate<-AllShapes_CENTER_X, 0, -AllShapes_CENTER_Y>
    rotate <-90,0,0>
    scale logo_box_side / AllShapes_WIDTH
    translate<-logo_box_side*.6, -logo_box_side*.6, 0>
}
#include "valve.pov"
object {
    AllShapes_Z
    translate<-AllShapes_CENTER_X, 0, -AllShapes_CENTER_Y>
    rotate <-90,0,0>
    scale logo_box_side / AllShapes_WIDTH
    translate<logo_box_side*.6, -logo_box_side*.6, 0>
}
#break
#end
#end
#end



/*#########################################################################
# End of File
#########################################################################*/
